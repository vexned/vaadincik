vaadin-cik
==============

Pilot project for CIK map.

Uses:
- Vaadin for main page;
- V-Leaflet [vaadin plugin](https://vaadin.com/directory/component/v-leaflet);
- Apache Spark to load data and cluster points with spark.mllib.clustering.KMeans;
- SQLite database of [points](https://www.cikrf.ru).

Check out screenshots example at 'screenshots' directory.

![](screenshots/moscow.png)

Workflow
========

To prepare database run '7zr e cik_20180215.7z' at 'base' directory.
Use sqlite3 to view data.

To compile the entire project, run "mvn install".

To run the application, run "mvn jetty:run" and open http://localhost:8080/ .

To produce a deployable production mode WAR:
- change productionMode to true in the servlet class configuration (nested in the UI class)
- run "mvn clean package"
- test the war file with "mvn jetty:run-war"

SQLite
======

```
$ 7zr e cik_20180215.7z
$ sqlite3 base/cik.sqlite
sqlite> .help
sqlite> .tables
cik_people cik_people_reserve cik_uik
sqlite> .quit
```
