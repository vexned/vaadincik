package org.eltex;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.leaflet.shared.Bounds;

import java.util.*;

/**
 * Init Spark context.
 * Load data from SQLite.
 * Cluster points.
 */
public class SparkApi {

    public static final Logger logger = LoggerFactory.getLogger(SparkApi.class);

    // https://spark.apache.org/docs/latest/configuration.html
    private final static SparkConf conf = new SparkConf(false)
            .setMaster("local[*]")
            .setAppName("vaadincik")
            .set("spark.logConf", "true")
            .set("spark.driver.memory", "4g")
            ;

    private final static SparkSession spark = SparkSession
            .builder()
            .config(conf)
            .appName("vaadincik")
            .getOrCreate();

    private final static SQLContext sqlContext = new SQLContext(spark);

    private final static Dataset<Row> dataset = sqlContext
            .read()
            .format("jdbc")
            .option("url", "jdbc:sqlite:base/cik.sqlite")
            .option("dbtable", "cik_uik")
            .load();

    /**
     * Get total points count at base.
     * @return points count
     */
    static long getTotal() {
        return dataset.count();
    }

    /**
     * Get region points count (accessible at map now).
     * @param bounds map bounds
     * @return points count
     */
    static long getRegion(Bounds bounds) {
        long start = System.currentTimeMillis();
        String filter = String.format("lat_ik >= %f and lat_ik <= %f and lon_ik >= %f and lon_ik <= %f",
                bounds.getSouthWestLat(), bounds.getNorthEastLat(),
                bounds.getSouthWestLon(), bounds.getNorthEastLon());

        long count = dataset.filter(filter).count();
        logger.debug("Count {} by {} ms", count, System.currentTimeMillis()-start);
        return count;
    }

    /**
     * Get region points clusters with aggregated data.
     * @param bounds map bounds
     * @return array of clusters
     */
    static ArrayList<ClusterAp> getClusters(Bounds bounds) {

        long start = System.currentTimeMillis();

        String filter = String.format("lat_ik >= %f and lat_ik <= %f and lon_ik >= %f and lon_ik <= %f",
                bounds.getSouthWestLat(), bounds.getNorthEastLat(),
                bounds.getSouthWestLon(), bounds.getNorthEastLon());

        JavaRDD<Row> data = dataset.select("lat_ik", "lon_ik").filter(filter).toJavaRDD();
        if (data.count()==0) {
            return null;
        }

        logger.debug("Select by {} ms", System.currentTimeMillis()-start);
        start = System.currentTimeMillis();

        JavaRDD<Vector> parsedData = data.map(s -> {
            return Vectors.dense(s.getFloat(0), s.getFloat(1));
        });
//        parsedData.cache(); don't cache

        int numClusters = 15;
        int numIterations = 30;

        KMeansModel clusters = KMeans.train(parsedData.rdd(), numClusters, numIterations);

        logger.debug("KMeans.train by {} ms", System.currentTimeMillis()-start);
        start = System.currentTimeMillis();

        ArrayList<ClusterAp> aps = new ArrayList<>();
        for (Vector center: clusters.clusterCenters()) {
//            logger.debug("Cluster: {}", center);
            ClusterAp ap = new ClusterAp(center.apply(0), center.apply(1));
            aps.add(ap);
        }

        data.toLocalIterator().forEachRemaining(r -> {
            int i = clusters.predict(Vectors.dense(r.getFloat(0), r.getFloat(1)));
            ClusterAp ap = aps.get(i);
            ap.appendSize();
            ap.appendSum(11);
        });

        logger.debug("Iterate by {} ms", System.currentTimeMillis()-start);

        return aps;
    }
}
