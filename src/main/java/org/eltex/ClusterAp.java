package org.eltex;

import java.io.Serializable;

/**
 * Map market object
 */
public class ClusterAp implements Serializable {
    // coordinates
    private double lat;
    private double lon;
    // point in cluster
    private int size=0;
    // users connected to cluster points
    private int sum=0;

    public ClusterAp(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public int getSize() {
        return size;
    }

    public void appendSize() {
        this.size+=1;
    }

    public int getSum() {
        return sum;
    }

    public void appendSum(int sum) {
        this.sum+=sum;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "ClusterAp{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", size=" + size +
                ", sum=" + sum +
                '}';
    }
}
