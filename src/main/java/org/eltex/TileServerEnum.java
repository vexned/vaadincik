package org.eltex;

import org.vaadin.addon.leaflet.shared.Crs;

/**
 * Some tiles servers.
 */
public enum TileServerEnum {
    // http://wiki.openstreetmap.org/wiki/Tile_servers
    OpenStreetMap("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            false,
            "OpenStreetMap",
            Crs.EPSG3857,
            "a", "b", "c"),
    OpenCycleMap("http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            false,
            "OpenCycleMap",
            Crs.EPSG3857,
            "a", "b"),
    // Help: https://tech.yandex.ru/maps/doc/staticapi/1.x/dg/concepts/input_params-docpage/
    // https://habrahabr.ru/post/283354/
    YandexMap("https://vec{s}.maps.yandex.net/tiles?l=map&z={z}&x={x}&y={y}&lang=ru",
            true,
            "Yandex.Maps",
            Crs.EPSG3395,
            "01", "02", "03", "04"),
    // Sputnik
    Sputnik("http://tiles.maps.sputnik.ru/{z}/{x}/{y}.png",
            false,
            "Sputnik",
            Crs.EPSG3857)
    ;

    private String url;
    private String label;
    private String[] subdomains;
    private boolean active;
    private Crs crs;

    TileServerEnum(String url, boolean active, String label, Crs crs, String ...subdomains) {
        this.url = url;
        this.active = active;
        this.label = label;
        this.subdomains = subdomains;
        this.crs = crs;
    }

    public String getUrl() {
        return url;
    }

    public boolean isActive() {
        return active;
    }

    public String getLabel() {
        return label;
    }

    public String[] getSubdomains() {
        return subdomains;
    }

    public Crs getCrs() {
        return crs;
    }
}
