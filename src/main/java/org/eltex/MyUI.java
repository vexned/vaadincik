package org.eltex;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.leaflet.LCircleMarker;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.shared.Point;
import org.vaadin.addon.leaflet.shared.TooltipState;

import javax.servlet.annotation.WebServlet;

import java.util.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private final LMap map = new LMap();
    private final TextField region = new TextField();
    private final TextField zoom = new TextField();

    private static final Logger logger = LoggerFactory.getLogger(MyUI.class);

    @Override
    protected void init(VaadinRequest vaadinRequest) {

//        System.out.println(getFile("logback.xml"));

        logger.debug("Started");

        // print internal state
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        StatusPrinter.print(lc);

        final VerticalLayout layout = new VerticalLayout();
        
        final Label label = new Label("Move map, please:");

        map.setCenter(Const.nsk_lat, Const.nsk_lon);
        map.setZoomLevel(15);
        map.setWidth("100%");
        map.setHeight("500px");

        TileServerEnum tileServer = TileServerEnum.Sputnik;

        final LTileLayer baseLayer = new LTileLayer();
        baseLayer.setUrl(tileServer.getUrl());
        baseLayer.setAttributionString(tileServer.getLabel());
        baseLayer.setActive(true);
        baseLayer.setSubDomains(tileServer.getSubdomains());

        map.addLayer(baseLayer);
        map.setCrs(tileServer.getCrs());
        map.addMoveEndListener(e -> reload());

        final TextField total = new TextField();
        total.setCaption("Total:");
        total.setValue(Long.toString(SparkApi.getTotal()));

        region.setCaption("Region:");
        zoom.setCaption("Zoom:");
        
        layout.addComponents(label, map, total, region, zoom);
        
        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

    /**
     * Reload page
     * Get data from base, cluster points, put markers
     */
    private void reload() {
        cleanUp();

        logger.debug("Reload {}, zoom {}", map.getBounds(), map.getZoomLevel());

        region.setValue(Long.toString(SparkApi.getRegion(map.getBounds())));
        zoom.setValue(Double.toString(map.getZoomLevel()));

        ArrayList<ClusterAp> clusters = SparkApi.getClusters(map.getBounds());
        if (clusters!=null) {
            clusters.forEach(this::addCluster);
        }
    }

    /**
     * Add map marker for cluster
     * @param ap Single cluster data
     */
    private void addCluster(ClusterAp ap) {

        final LCircleMarker marker = new LCircleMarker();
        marker.setId(Const.MARK_ID);
        marker.setPoint(new Point(ap.getLat(), ap.getLon()));
        marker.setRadius(20);

        TooltipState state = new TooltipState();
        state.permanent=true;
        state.opacity=0.7;
        marker.setTooltipState(state);

        StringBuilder sb = new StringBuilder();
        if (ap.getSize()>1) {
            sb.append(ap.getSize()).append(" ");
        }
        sb.append("[").append(ap.getSum()).append("]");

        marker.setTooltip(sb.toString());

        map.addComponent(marker);
    }

    /**
     * Clear markers on map.
     */
    private void cleanUp() {
        final LinkedList<Component> l = new LinkedList<Component>();

        // Adds all components
        map.iterator().forEachRemaining(c -> {
            if (Const.MARK_ID.equals(c.getId())) {
                l.add(c);
            }
        });

        // Removes all component
        l.iterator().forEachRemaining(map::removeComponent);
    }
}
