package org.eltex;

public class Const {
    /*
     * https://www.distancesto.com/coordinates/ru/novosibirsk-latitude-longitude/history/4461.html
     * Novosibirsk Latitude: 	55.00835259999999
     * Novosibirsk Longitude: 	82.93573270000002
     */
    public static final double nsk_lat = 55.00835259999999;
    public static final double nsk_lon = 82.93573270000002;

    public static final String MARK_ID = "MARK_ID";
}
